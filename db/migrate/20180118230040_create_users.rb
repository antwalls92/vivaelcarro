class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :email
      t.string :image
      t.belongs_to :role, foreign_key: true
      t.string :address

      t.timestamps
    end
  end
end
