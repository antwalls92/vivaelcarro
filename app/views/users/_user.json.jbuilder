json.extract! user, :id, :username, :password, :email, :image, :role_id, :address, :created_at, :updated_at
json.url user_url(user, format: :json)
